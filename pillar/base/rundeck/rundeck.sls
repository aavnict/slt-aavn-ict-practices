rundeck:
  db_image:
    version: 10.3.2
    db_name: rundeckdb
    root_password: "abc123"
    user: rundeck
    user_password: "abc123"
  rundeck_image:
    name: "jbarahona/rundeck"
    version: "3.0.8_upg"
    login_module: "RDpropertyfilelogin"
    external_server_url: "rundeck-ict-{{ grains['env'] }}.aavn.local"
    id_rsa_pub: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDT8+dvidraVXg0q584qz63wyr7kRlyiaFiy7/E/6dtOUNWsroKdCb7EyL4YnKbo9Sot0dioYLE+/YorBd5TGZ2kyAipD8rpO5Xe6sAWhDcYU9tSXXLWfHG2ZbFHvnYx6yD9033D+/kdPu9KUU7dCnQEcyt7Y3qlNf41N5qEXPEF0/VyWeateI+BOhH5orA2cJqfcW6xvh/udlrpNJv57aHFW+7VdDWClTIvfmzOZL4jtV0iI6tO7rDa9ox+Ezlg5vH5JTnWiYDEqnA8WPVcYD1ETJ9GLnZUl/Hxuwo9TGX/OCkX+zRzSN8Qt2xwHigvCIZTeTWXZA8Ge1P4RzTlIL7M/j9YrPOl0wv5y4UXAhChsQ53R4BFIYdJhVvJDxM4O9Yd+msBpxk1li+WwM+l3UyzNJbk3K+7My7J9bF1D8hkZuziio9iz4F0Wn17znkrPlUSB6oT1AE+14uFX4i1IFNpOiB9ksTQQ0br3cQM8GQrIo+pGW6rUpzxa1jWGLcHW1TyDh0cFHRTPinRE2HghbZ1hb+bCjO6JZRQkW+H/yODfiCz36kDfpM+ImHKDFXg0+rdx2WSp+N6hblmpZ5Tz1MNuz07WL6zRBRmqr1RJ6dO9Zm4qPhZfOcxEN0wIt5R/FrXh42RWyDOH+hvznwMVuIZ4tfH98mXzdLFbLvXfQRMQ== tin.tranvan@axonactive.com"
    id_rsa: |
      -----BEGIN RSA PRIVATE KEY-----
      MIIJJwIBAAKCAgEA0/Pnb4na2lV4NKufOKs+t8Mq+5EZcomhYsu/xP+nbTlDVrK6
      CnQm+xMi+GJym6PUqLdHYqGCxPv2KKwXeUxmdpMgIqQ/K6TuV3urAFoQ3GFPbUl1
      y1nxxtmWxR752Mesg/dN9w/v5HT7vSlFO3Qp0BHMre2N6pTX+NTeahFzxBdP1cln
      mrXiPgToR+aKwNnCan3Fusb4f7nZa6TSb+e2hxVvu1XQ1gpUyL35szmS+I7VdIiO
      rTu6w2vaMfhM5YObx+SU51omAxKpwPFj1XGA9REyfRi52VJfx8bsKPUxl/zgpF/s
      0c0jfELdscB4oLwiGU3k1l2QPBntT+Ec05SC+zP4/WKzzpdML+cuFFwIQobEOd0e
      ARSGHSYVbyQ8TODvWHfprAacZNZYvlsDPpd1MszSW5NyvuzMuyfWxdQ/IZGbs4oq
      PYs+BdFp9e855Kz5VEgeqE9QBPteLhV+ItSBTaTogfZLE0ENG693EDPBkKyKPqRl
      uq1Kc8WtY1hi3B1tU8g4dHBR0Uz4p0RNh4IW2dYW/mwozuiWUUJFvh/8jg34gs9+
      pA36TPiJhygxV4NPq3cdlkqfjeoW5ZqWeU89TDbs9O1i+s0QUZqq9USenTvWZuKj
      4WXznMRDdMCLeUfxa14eNkVsgzh/ob858DFbiGeLXx/fJl83SxWy7130ETECAwEA
      AQKCAgB6Rx9A8QYKqf50XIK6HclMGNOGWLqWoqIxrhxPmbJMyBl6PhgoicQnW230
      YE+ATS8Vus77LvSTkkz8P/Q27YW7aLvEYH91PWynmdgYlt1dVuKMyTGlBJoAv21F
      Ac7gaqmzZIxdOZuLcJX/Fn0zhJvOx0YPHZqfOAm1K5G5HaJtAYn0a4JS9hW08/3R
      5UYtZjPMXg0h6UJZ5a1ydZ9WIhbirdJNAIA/y6wIN9mDtXZ94WFyHoo5lNj1fxZd
      d8NdMeVAmDXzlsWRRkYa4uhWrmnYeHhpRUeLAgPx7Gg4ZOScq64qBpFIpJGRUb42
      mzS8fm0prg+UQFxC4C5RxCxuQCKjnx1utWs/1sup76oMQY2B+TlHIkICG0lj1TZ2
      LZux+VhQTpIke0Nub6uPlLc5Qp+Mxvo4HrmNiLlyLl2ajsJpsoS5siOLQhSbFWTD
      UDvfqvv8NJLvF5Y4sYKwcfZhesIwPgU5YmtNeFfjzEo034jCFPhAZQPfmwltUqJO
      /U2WgzZx49MCMr5WNC8eS1WsxfjqI+A/qN8iXVDaNQeB9ZYISqQ4Gmmcirt9hLYH
      Mo55bVaXFBJHdqE1WfsqBfaEoOmIzcNuPvIerwpuZkPzEQ8uQXAgIKwkYltI7x7G
      R0AJp86Hcxfc+wQJ7HwBD9XyOUZ9WWSdWbSSAzFqcgc4cxCdcQKCAQEA6VhE1bVF
      e/+mAbtsbMcJ48S+agZmw46HAyyMRAz9wcAqpn5G1HP1Z3g+M6BMjV87I3MVaJlJ
      tr9IkqLSs+9c8BONf7wK99ZATLFeuqoPYDd23WN8/xfM3QItqzqUuc+JCh7JJODB
      iO/CTBm0sA522FPG1DQAb4tXqMKGPFpPLSddzL4qIJjDunY3KtumU5RhSXgEJ2/1
      NqMiGVum2IZCrsQv7vwaKArHbyq+/fZPm/gdfvRbaHQxm3D4ws7v18AllcoBvpAw
      bkag41/slPBP8nl7ez/mDt8u0zoyEkimOlpe3wzdHzm9WUUxB+Mch9blMnb7+QGi
      win8vi1ISx7LDQKCAQEA6Ifw0Vb7VLufmfrVFeJbk4SfTfP3GHe0Qz7TiE51Ol/K
      GO4x18YtqrtED3rYyPp2H9sQuam29FdhIPgB9ywFhEkagQw73jRB9B3Uq90HUvK+
      GQvd4oeBFxJCpLRZHU2uUEH/PM9t75KLl8xdZ8B/DHFPrHgj8SJET6ymEJ7dfjYf
      g1e+wHfEwvWwQBfcL+nQYevWQpc0TMcu/A1y0hv9+epYzdMBCUJSKiQV9xJ0AICh
      cdhEXQJBNXoxKruHOQGSIOvxexoCUt0ShD0EFrmiW8HBHUZU7dJwRJqyN5ew8wGe
      4p4pX+72iZj2+9OWJ3Ahjza5Yi2+fp7qh4ej2URFtQKCAQAHZSnBUB5koKSFN4hn
      JiO1GT4jpBJLrk+vFRFY8sR0CIARiLz77LThU5k9D559eIAqTnJmPz1r0kIdi5V7
      6GEcprH8U1xRoHOnOpPS/KLCHN/igk2q0/lFI9WIsLGX9fDU5kbfqVPY4ChbOxFT
      5xFzJ6yMex1sa5EaoxeYABC0UKQTYg95uv2xAiAbdpyd130B1BDloxG1apQhE5uF
      DzQ7z7HpNAosLLNHXaZx7AV33vQoWtwuwkf0Mi9gFSjawJcwWReXD4DJbemZayfX
      TIpsF9eG/25uGDqmrRh7JdxGyyhoj0LOw4ixtJSqOFNmJrRa1hSEPr4Gw0fkSxEY
      eGBdAoIBADzWzIhJJMsfp/uGqFfoeKNZMMEZks4BC9YISxo/xOY4B82MDNhyzqH8
      Cyutraiv91YQYAjjwa2eaxhQK1t7VhotclTPWZ6it5ribzFkTDVEs74j37MUVw8t
      ROiJE0PafkAbdGd5sj7bbdCStOGfeN5Ni1YJvQWrPIpIFs8EcdlC2e3oeE0tSPrl
      g27Rj1et6kqkhNO5cgKgnsdt0Vvt3P6VdRStH3lHs+24sVKKrnUvMo5x0jkvjNYs
      GU8f75pXsd8RPIZD4NXDZ8PXnwk/Gp69TASX0ULEK4GXpAdtleuOwf5y+UpTYIfR
      0NVEn+XDrdQlsk1gwJWqiJxbPeBrv30CggEALi12EfkDdT7M3R7MswXnFHWEcJ5L
      vIewNkHf6b0F638PJ9dfVuT+m6QZgpRDMVjmgeLMPoWu4TRn3AIVKBIfuxnaSse3
      2c2eYp5YD3wIvLQyj05UITNPde8NULNyG+/ZGUyhMn19dQHo72RyX8PR2RArpSU1
      z8BQGPJMISAG91BOKghCBQuD/J/NIWw8W+ANp+MTI/vfnau60CX09ACtvlT7L+iD
      OXrlv5hdbCundkT7yVlKt8QM8shWWTLJW2BG+VBat1iZl5jWzKwYU7zT31Wlz2sx
      ZLYg5KDrvq8WFHTTohXjRtDcvo0ZQIn8geYYKU5QnO8lv7BE8VaodDhGYg==
      -----END RSA PRIVATE KEY-----


