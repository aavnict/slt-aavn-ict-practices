docker:
  container_user:
    password: "Aavn1234"
  docker_engine:
    new_install: True
    version: 18.03.0 # Default is latest version
    users: []
  docker_daemon:
    opts:
      dns:
        - 8.8.8.8
        - 8.8.4.4
      host:
        - tcp://0.0.0.0:2375
        - unix:///var/run/docker.sock
      userns-remap:
        - container-user
    env_vars:
      DOCKER_HOST: /var/run/docker.sock
      TLS_VERIFY: TRUE