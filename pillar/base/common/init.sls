#!yaml|gpg
time_zone: 'Europe/Zurich'

# vars for packages.yml
update_exclude_packages: docker*

pkgs:
 basic:
    - epel-release
    - redhat-lsb-core
    - vim-enhanced
    - git
    - sudo
    - wget
    - yum-cron
    - mysql-server
 utils:
    - htop
    - bind-utils
    - net-tools
    - nmap
    - s3cmd
    - python2-pip
    - python2-psutil
    - traceroute
    - jq

# vars for zsh.yml
zsh:
 users:
    - root
    - cloud-user
 dir: "zsh/custom.zip"
 file: "zsh/.zshrc"
 repo: "git://github.com/robbyrussell/oh-my-zsh.git"



########################################## Variables must be defined ###################################

# vars for user.yml
users:
 root:
   password: "Aavn1234"
   auth_key: |
     'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDaMrRYFUgg0d4b/EKg1IDR+p8tSgwIGs1LxuKw0cq0q116+Rg9n/nPWvW9nVSSEO6rjASbYUTk7MYb7hbqt3WHGRQzt9P6oEOpquD/gtovXZVWteO9lfDdp64y2HjkwUHTg8qwI3rGbgN8eUNS3YLWBGKPUOWk0N7jCROH5/hxhw=='
 cloud_user:
   password: "AAvn1234"
   auth_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRo8SnRAjzVbTjUqO84Pl3UjedcjXHPmgWslY8VWu56vqPlhw0U/MK8L5YCkpXqlpR3RGygQIVNyWQ0Tws2RbvMsR5wTm8gUNc0+gemGrPqbLkvwiZtwo+JOFRYTauIvkdytyK+q1Os/gep++81HVPavp3al7YxK9MdFh1ykrhn4hFD0oosTZ4H1/+zMbIkDqcXeNTYMZ5G9hHcCLkFxJUk8oxO6vqidJiUP6/N8/zSIRU8kiznNFhSkDMEhFL1z1nlyKkW6xZ/hMBFmQVP47q0v3e701n9tXP8qwjGTlpDLiJ/XVt7innQCp/rNYJ20iME5AcZwShMMtddQrVM1ev oregon-key"
 container_user:
   password: "Aavn1234"
