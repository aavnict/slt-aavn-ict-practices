docker_engine_daemon_file:
  file.managed:
    - name: "/etc/systemd/system/docker.service.d/docker-opts.conf"
    - source: "salt://docker/files/docker-opts.conf.jinja"
    - template: jinja
    - makedirs: True
    - user: root
    - group: root
    - mode: 0644

daemon_reload:
  cmd.r
    - name: systemctl daemon-reload
    - watch:
      - file: docker_engine_daemon_file

docker_ce_restart:
  service.running:
    - name: docker
    - enable: True
    - reload: True
    - watch:
      - pkg: docker_installing
      - file: docker_engine_daemon_file