{%- set timezone = salt['pillar.get']('time_zone') %}
set timezone:
 timezone.system:
  - name: {{ timezone  }}
