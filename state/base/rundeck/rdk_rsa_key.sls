{% set rundeck_uid = salt['file.get_uid']('/opt/containers/var/lib/rundeck/ssh') %}
{% set rundeck_gid = salt['file.get_gid']('/opt/containers/var/lib/rundeck/ssh') %}

rundeck_public_key:
  file.managed:
    - name: "/opt/containers/var/lib/rundeck/ssh/id_rsa.pub"
    - user: {{ rundeck_uid }}
    - group: {{ rundeck_gid }}
    - mode: 0600
    - makedirs: True
    - contents: "{{ pillar['rundeck']['rundeck_image']['id_rsa_pub'] }}"

rundeck_private_key:
  file.managed:
    - name: "/opt/containers/var/lib/rundeck/ssh/id_rsa"
    - user: {{ rundeck_uid }}
    - group: {{ rundeck_gid }}
    - mode: 0600
    - contents: "{{ pillar['rundeck']['rundeck_image']['id_rsa'] }}"
    - makedirs: True