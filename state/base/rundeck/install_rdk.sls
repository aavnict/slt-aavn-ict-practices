pull-db-image:
  docker_image.present:
    - name: "mariadb:{{ pillar['rundeck']['db_image']['version'] }}"
    - force: True

pull-rundeck-image:
  docker_image.present:
    - name: "{{ pillar['rundeck']['rundeck_image']['name'] }}:{{ pillar['rundeck']['rundeck_image']['version'] }}"
    - force: True

running-db-container:
  docker_container.running:
    - name: rundeck-db
    - image: "mariadb:{{ pillar['rundeck']['db_image']['version'] }}"
    - force: True
    - binds:
      - "/opt/containers/var/lib/mysql:/var/lib/mysql"
    - restart_policy: always
    - port_bindings:
      - "127.0.0.1:3306:3306"
    - environment:
      - TZ: "Europe/Zurich"
{% if salt['file.directory_exists']('/opt/containers/var/lib/mysql/mysql') == False %}
      - MYSQL_ROOT_PASSWORD: "{{ pillar['rundeck']['db_image']['root_password'] }}"
      - MYSQL_USER: "{{ pillar['rundeck']['db_image']['user'] }}"
      - MYSQL_PASSWORD: "{{ pillar['rundeck']['db_image']['user_password'] }}"
      - MYSQL_DATABASE: "{{ pillar['rundeck']['db_image']['db_name'] }}"
{% endif %}
    - require:
        - docker_image: pull-db-image

running-rundeck-container:
  docker_container.running:
    - name: rundeck
    - image: "{{ pillar['rundeck']['rundeck_image']['name'] }}:{{ pillar['rundeck']['rundeck_image']['version'] }}"
    - force: True
    - binds:
      - "/opt/containers/var/rundeck:/var/rundeck"
      - "/opt/containers/var/log/rundeck:/var/log/rundeck"
      - "/opt/containers/etc/rundeck:/etc/rundeck"
      - "/opt/containers/var/lib/rundeck/ssh:/var/lib/rundeck/.ssh"
      - "/opt/containers/var/lib/rundeck/logs:/var/lib/rundeck/logs"
      - "/opt/containers/opt/rundeck-plugins:/opt/rundeck-plugins"
    - restart_policy: always
    - port_bindings:
      - "8080:4440"
    - links:
      - rundeck-db:rundeck-db
    - environment:
      - RDECK_JVM_SETTINGS: "-Xmx4096m -Xms1024m -XX:MaxMetaspaceSize=1024m -server -Dfile.encoding=UTF-8"
      - RUNDECK_STORAGE_PROVIDER: db
      - RUNDECK_PROJECT_STORAGE_TYPE: db
      - RUNDECK_PASSWORD: "{{ pillar['rundeck']['db_image']['user_password'] }}"
      - DATABASE_ADMIN_USER: root
      - DATABASE_ADMIN_PASSWORD: "{{ pillar['rundeck']['db_image']['root_password'] }}"
      - DATABASE_URL: "jdbc:mysql://rundeck-db/rundeckdb?autoReconnect=true"
      - NO_LOCAL_MYSQL: true
      - SERVER_URL: "https://{{ grains['host'] }}"
      - EXTERNAL_SERVER_URL: "https://{{ pillar['rundeck']['rundeck_image']['external_server_url'] }}"
      - LOGIN_MODULE: "{{ pillar['rundeck']['rundeck_image']['login_module'] }}"
      - TZ: "Europe/Zurich"
    - require:
      - docker_image: pull-rundeck-image